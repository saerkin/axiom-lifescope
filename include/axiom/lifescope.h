/******************************************************************************/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2018 Sergey A Erkin                                          */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom          */
/* the Software is furnished to do so, subject to the following conditions:   */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included    */
/* in all copies or substantial portions of the Software.                     */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#pragma once

#include <axiom/set.h>
#include <axiom/iterator.h>
#include <cassert>

namespace axiom {

/**
 * Slave object which life is controlled by LifeScope
 */
class LifeScopeObject {
private:
  friend class LifeScope;

protected:
  explicit LifeScopeObject(LifeScope& lifeScope);

private:
  LifeScopeObject(const LifeScopeObject& other);
  LifeScopeObject& operator=(const LifeScopeObject& other);

protected:
  virtual ~LifeScopeObject() {}
};

/**
 * Master object who destruct slave objects when its life is over
 */
class LifeScope {
private:
  friend class LifeScopeObject;

private:
  set<LifeScopeObject*> objects;

// Constructors
public:
  LifeScope() {}

  ~LifeScope() {
    iterator<LifeScopeObject*> it = objects.begin();
    while (it.hasNext())
      delete it.next();
  }

private:
  LifeScope(const LifeScope&);
  LifeScope& operator=(const LifeScope&);

// Methods
private:
  void put(LifeScopeObject* pObject) {
    assert(pObject);
    objects = objects.add(pObject);
  }
};

inline LifeScopeObject::LifeScopeObject(LifeScope& lifeScope) {
  lifeScope.put(this);
}

} // end of axiom namespace