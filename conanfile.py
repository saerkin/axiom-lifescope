# noinspection PyUnresolvedReferences
from conans import ConanFile

class AxiomLifescopeConan(ConanFile):
  name = "axiom-lifescope"
  version = "1.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/lifescope.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  no_copy_source = True
  generators = "cmake_multi"
  requires = \
    "axiom-collection/0.1.0@sergey-erkin/experimental"
  build_requires = \
    "Catch/1.12.2@bincrafters/stable"

  def package(self):
    self.copy("*.h")

  def package_id(self):
    self.info.header_only()
