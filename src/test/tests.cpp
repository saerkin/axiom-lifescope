#include <catch.hpp>
#include "axiom/lifescope.h"

using namespace axiom;

class ObjectWithDeathMarker : LifeScopeObject {
private:
  bool& marker;

public:
  ObjectWithDeathMarker(bool& marker, LifeScope& lifescope)
    : LifeScopeObject(lifescope), marker(marker) {
    marker = false;
  }

private:
  virtual ~ObjectWithDeathMarker() {
    marker = true;
  }
};

TEST_CASE("LifeScope destructor test") {
  bool objectIsDead = false;
  {
    LifeScope lifeScope;
    new ObjectWithDeathMarker(objectIsDead, lifeScope);
  }
  REQUIRE(objectIsDead);
}